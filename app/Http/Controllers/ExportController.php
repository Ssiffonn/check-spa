<?php

namespace App\Http\Controllers;

use App\Exports\HistoryExport;
use App\Services\ExportService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{
    public function __construct(ExportService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        return $this->service->index();
    }

    public function export()
    {
        return $this->service->export();
    }
}
