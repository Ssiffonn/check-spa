<?php

namespace App\Http\Controllers;

use App\Services\CheckService;
use Illuminate\Http\Request;

class CheckController extends Controller
{
    public function __construct(CheckService $service)
    {
        $this->service = $service;
    }

    public function check(Request $request)
    {
        return $this->service->check($request->domains);
    }
}
