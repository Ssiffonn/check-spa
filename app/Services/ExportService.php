<?php

namespace App\Services;

use App\Exports\HistoryExport;
use App\Models\History\History;
use Maatwebsite\Excel\Facades\Excel;

class ExportService
{
    public function index()
    {
        return History::select([
            'url',
            'laravel'
        ])->get();
    }

    public function export()
    {
        return Excel::download(new HistoryExport(), 'Сайты.xlsx');
    }
}
