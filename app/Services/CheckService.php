<?php

namespace App\Services;

use App\Exports\HistoryExport;
use App\Jobs\StartCheckJob;

class CheckService
{
    public function check($domains)
    {
        StartCheckJob::dispatch($domains);
        return true;
    }
}
