<?php

namespace App\Jobs;

use App\Models\History\History;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $domain;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($domain)
    {
        $this->domain = $domain;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $regExp = '/[^\s]+(?:_session)\b/';
        $client = new Client(['cookies' => true]);
        $client->request('GET', $this->domain);
        $cookies = $client->getConfig('cookies');
        $cookies->toArray();
        foreach ($cookies as $cookie) {
            $cookieArray = $cookie->toArray();
            $match = preg_match($regExp, $cookieArray['Name']);
            if ($match) {
                History::firstOrCreate(['url' => $this->domain], ['laravel' => History::RESULT[1]]);
            }
        }
        History::firstOrCreate(['url' => $this->domain], ['laravel' => History::RESULT[0]]);
    }
}
