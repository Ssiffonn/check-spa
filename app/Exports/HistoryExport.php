<?php

namespace App\Exports;

use App\Models\History\History;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithTitle;

class HistoryExport implements FromQuery, WithHeadings, WithStyles, ShouldAutoSize, WithTitle
{
    public function headings(): array
    {
        return [
            'URL',
            'Laravel'
        ];
    }
    public function styles(Worksheet $sheet)
    {
        return [
            1 => ['font' => ['bold' => true]],
        ];
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {
        return History::query()
            ->select([
                'url',
                'laravel'
            ])->where('laravel', '=', History::RESULT[1]);
    }
    public function title(): string
    {
        return 'Сайты';
    }
}
