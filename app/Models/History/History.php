<?php

namespace App\Models\History;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    use HasFactory;

    public const RESULT = [
        0 => 'Не использует',
        1 => 'Использует'
    ];

    protected $fillable = [
        'url',
        'laravel'
    ];
}
